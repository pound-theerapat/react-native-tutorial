import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ImageDetail from '../component/ImageDetail';
const ImageScreen = () => {
  return (
    <View>
      <ImageDetail
        title="Forest"
        imageSource={require('../../asset/forest.jpg')}
        imageScore="9"
      />

      <ImageDetail
        title="Beach"
        imageSource={require('../../asset/beach.jpg')}
        imageScore="7"
      />
      <ImageDetail
        title="Mountain"
        imageSource={require('../../asset/mountain.jpg')}
        imageScore="4"
      />
    </View>
  );
};

const styles = StyleSheet.create({});

export default ImageScreen;
