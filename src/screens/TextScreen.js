import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';

const TextScreen = () => {
  const [name, setName] = useState('');
  const [Password, setPassword] = useState('');
  return (
    <View>
      <Text style={styles.titleText}>Enter your name:</Text>
      <TextInput
        style={styles.input}
        autoCapitalize="none"
        value={name}
        onChangeText={newValue => setName(newValue)}
      />
      <Text style={styles.resultText}>Hello, My name is: " {name} "</Text>
      <Text style={styles.titleText}>Enter your Password:</Text>
      <TextInput
        style={styles.input}
        autoCapitalize="none"
        value={Password}
        secureTextEntry={true}
        onChangeText={newValue => setPassword(newValue)}
      />
      {Password.length < 4 ? (
        <Text style={styles.passwordAlert}>
          Your password must more than 4 characters
        </Text>
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  input: {
    margin: 15,
    borderColor: 'black',
    borderWidth: 1,
    color: 'black',
    fontSize: 18,
  },
  passwordAlert: {
    marginLeft: 15,
    color: 'red',
    fontSize: 16,
  },
  titleText: {
    marginLeft: 15,
    marginTop: 15,
    fontSize: 22,
    fontWeight: 'bold',
  },
  resultText: {
    marginLeft: 15,
    fontSize: 20,
  },
});
export default TextScreen;
