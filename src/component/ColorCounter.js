import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const ColorCounter = ({color, onIncrease, onDecrease}) => {
  return (
    <View>
      <Text style={styles.txtStyle}>{color}</Text>
      <Button onPress={() => onIncrease()} title={`Increase more ${color}`} />
      <Button onPress={() => onDecrease()} title={`Decrease more ${color}`} />
    </View>
  );
};

const styles = StyleSheet.create({
  txtStyle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
export default ColorCounter;
