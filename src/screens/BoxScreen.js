//Box object model & Flexbox & Position
import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const BoxScreen = () => {
  return (
    <View style={styles.parentStyle}>
      <View style={styles.viewOneStyle} />
      <View style={styles.viewTwoStyle} />
      <View style={styles.viewThreeStyle} />
      {/* <Text style={styles.textOneStyle}>child #1</Text>
      <Text style={styles.textTwoStyle}>child #2</Text>
      <Text style={styles.textThreeStyle}>child #3</Text> */}
    </View>
  );
};

//Default is stretch
//View is Parent , Text is children
//alignItem let parent control children direction
//Default is stretch
//Parent property => [alignItem,justifyContent,flexDirection]
//Child property =>[flex,alignself]
const styles = StyleSheet.create({
  parentStyle: {
    borderWidth: 3,
    borderColor: 'black',
    height: 200,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  textOneStyle: {
    borderWidth: 3,
    borderColor: 'red',
    alignSelf: 'flex-start',
    // flex: 4,
  },
  textTwoStyle: {
    borderWidth: 3,
    borderColor: 'red',
    // left: 10,
    // right: 10,
    // bottom: 10,
    // ...StyleSheet.absoluteFillObject,
    // position: 'absolute',
    // alignSelf: 'stretch',
    // flex: 4,
  },
  textThreeStyle: {
    borderWidth: 3,
    borderColor: 'red',
    alignSelf: 'flex-end',
    // flex: 2,
  },
  viewOneStyle: {
    backgroundColor: 'red',
    height: 50,
    width: 50,
    // alignSelf: 'flex-start',
  },
  viewTwoStyle: {
    backgroundColor: 'green',
    height: 50,
    width: 50,
    alignSelf: 'center',
  },
  viewThreeStyle: {
    backgroundColor: 'blue',
    height: 50,
    width: 50,
  },
});
export default BoxScreen;
