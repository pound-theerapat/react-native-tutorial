import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ColorCounter from '../component/ColorCounter';

const SquareScreen = () => {
  const COLOR_INCREMENT = 20;
  const [red, setRed] = useState(0);
  const [blue, setBlue] = useState(0);
  const [green, setGreen] = useState(0);

  //Version Refactor code (Set RGB value limit, reduce redundant code)
  const setColor = (color, change) => {
    //color ==== 'red','green','blue
    //change === +15,=15
    switch (color) {
      case 'red': {
        red + change > 255 || red + change < 0 ? null : setRed(red + change);
        return;
      }
      case 'blue': {
        blue + change > 255 || blue + change < 0
          ? null
          : setBlue(blue + change);
        return;
      }
      case 'green': {
        green + change > 255 || green + change < 0
          ? null
          : setGreen(green + change);
        return;
      }
    }
  };
  return (
    <View>
      <ColorCounter
        onIncrease={() => setColor('red', COLOR_INCREMENT)}
        onDecrease={() => setColor('red', -1 * COLOR_INCREMENT)}
        // Ori. Version
        // onIncrease={() => setRed(red + COLOR_INCREMENT)}
        // onDecrease={() => setRed(red - COLOR_INCREMENT)}
        color="Red"
      />
      <ColorCounter
        onIncrease={() => setColor('blue', COLOR_INCREMENT)}
        onDecrease={() => setColor('blue', -1 * COLOR_INCREMENT)}
        // Ori. Version
        // onIncrease={() => setBlue(blue + COLOR_INCREMENT)}
        // onDecrease={() => setBlue(blue - COLOR_INCREMENT)}
        color="Blue"
      />
      <ColorCounter
        onIncrease={() => setColor('green', COLOR_INCREMENT)}
        onDecrease={() => setColor('green', -1 * COLOR_INCREMENT)}
        // Ori. Version
        // onIncrease={() => setGreen(green + COLOR_INCREMENT)}
        // onDecrease={() => setGreen(green - COLOR_INCREMENT)}
        color="Green"
      />
      <View
        style={[styles.box, {backgroundColor: `rgb(${red},${green},${blue})`}]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    height: 150,
    width: 150,
  },
});
export default SquareScreen;
